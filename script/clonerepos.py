"""Subida de las variables a los entornos del pais para MAPFRE ATHENEA """
import os
import json
import calendar
import time
from pathlib import Path
import boto3
import libs.lib_logs as lib_logs
import credentials
import network

HOME = str(Path.home())
LISTADIC = []
local_epoch = "a"
PASSBITBUCKET = input("Introduce la pass del usuario de bitbucket:")
#  PASSBITBUCKET = "Aaso1laml1fdp!"

def datalake(entorno):
    """Funcion para datalake """
    with open('./tmp/dataquality2-cfvars/'+entorno+'/s3-vars.json') as fichevars:
        variables = json.load(fichevars)
    variables["Parameters"]["anonymizationAwsAccountID"] = ANONYMIZATIONAWSACCOUNTID
    variables["Parameters"]["PROSecCicdAwsAccountID"] = PROSECCICDAWSACCOUNTID
    variables["Parameters"]["transformationAwsAccountID"] = TRANSFORMATIONAWSACCOUNTID 
    variables["Parameters"]["cdu01AwsAccountID"] = CDU01AWSACCOUNTID
    variables["Parameters"]["datalabAwsAccountID"] = DATALABAWSACCOUNTID

    with open('./tmp/dataquality2-cfvars/'+entorno+'/s3-vars.json', 'w') as fichevars2:
        json.dump(variables, fichevars2, indent=1)
    with open('./tmp/dataquality2-cfvars/'+entorno+'/monitoring-vars.json') as fichevars:
        variables = json.load(fichevars)
    variables["Parameters"]["OperatorEmailSNSBillingMapfreInfraCloud"] = credentials.OperatorEmailSNSBillingMapfreInfraCloud
    variables["Parameters"]["OperatorEmailSNSBillingParadigma"] = credentials.OperatorEmailSNSBillingParadigma
    variables["Parameters"]["AccountAWSAuditSec"] = ACCOUNTAWSAUDITSEC
    with open('./tmp/dataquality2-cfvars/'+entorno+'/monitoring-vars.json', 'w') as fichevars2:
        json.dump(variables, fichevars2, indent=1)

    if entorno == "pre":
        with open('./tmp/dataquality2-cfvars/'+entorno+'/network-vars.json') as fichevars:
            variables = json.load(fichevars)
        variables["Parameters"]["CidrVPC"] = network.datalakeprevpc
        variables["Parameters"]["CidrSubnetPrivateAws1"] = network.datalakepreaz0
    if entorno == "dev":
        with open('./tmp/dataquality2-cfvars/'+entorno+'/network-vars.json') as fichevars:
            variables = json.load(fichevars)
        variables["Parameters"]["CidrVPC"] = network.datalakedevvpc
        variables["Parameters"]["CidrSubnetPrivateAws1"] = network.datalakedevaz0
        variables["Parameters"]["CidrSubnetPrivateAws2"] = network.datalakedevaz1
    if entorno == "pro":
        with open('./tmp/dataquality2-cfvars/'+entorno+'/network-vars.json') as fichevars:
            variables = json.load(fichevars)
        variables["Parameters"]["CidrVPC"] = network.datalakeprovpc
        variables["Parameters"]["CidrSubnetPrivateAws1"] = network.datalakeproaz0
        variables["Parameters"]["CidrSubnetPrivateAws2"] = network.datalakeproaz1
        variables["Parameters"]["CidrSubnetPrivateAws3"] = network.datalakeproaz2

    with open('./tmp/dataquality2-cfvars/'+entorno+'/network-vars.json', 'w') as fichevars:
        json.dump(variables, fichevars, indent=1)


def anonymization(entorno):
    """Funcion para anonizacion"""

    ANONCANONICAL = listado("anonymization", entorno).get("canonical")
    DATALAKECANONICAL = listado("datalake", entorno).get("canonical")
    with open('./tmp/anonymization-cfvars/'+entorno+'/anonymization-vars.json') as fichevars:
        variables = json.load(fichevars)
    variables["Parameters"]["OperatorEMail"] = credentials.OperatorEmailSNSBillingParadigma
    variables["Parameters"]["extCrossAccountId"] = TRANSFORMATIONAWSACCOUNTID
    variables["Parameters"]["ProcCrossAccountId"] = TRANSFORMATIONAWSACCOUNTID
    UNECANONICALS = ANONCANONICAL+","+DATALAKECANONICAL+","+DATALABAWSCANONICALACCOUNTID
    variables["Parameters"]["procLambdaEnvVarCANONICALIDS"] = UNECANONICALS
    with open('./tmp/anonymization-cfvars/'+entorno+'/anonymization-vars.json', 'w') as fichevars:
        json.dump(variables, fichevars, indent=1)


    with open('./tmp/anonymization-cfvars/'+entorno+'/monitoring-vars.json') as fichevars:
        variables = json.load(fichevars)
    variables["Parameters"]["OperatorEmailSNSBillingMapfreInfraCloud"] = credentials.OperatorEmailSNSBillingMapfreInfraCloud
    variables["Parameters"]["OperatorEmailSNSBillingParadigma"] = credentials.OperatorEmailSNSBillingParadigma
    variables["Parameters"]["OperatorEmailSNSServiceAlert"] = credentials.OperatorEmailSNSServiceAlert
    variables["Parameters"]["AccountAWSAuditSec"] = ACCOUNTAWSAUDITSEC
    variables["Parameters"]["BucketTemplatesName"] = cf_template

    with open('./tmp/anonymization-cfvars/'+entorno+'/monitoring-vars.json', 'w') as fichevars:
        json.dump(variables, fichevars, indent=1)
def listado(account, env):
    """Funcion para listado de las cuentas del pais """

    for can in LISTADIC:
        if account == can.get("account"):
            if env == can.get("env"):
                return can


def transformacion(entorno):
    """Funcion para transformacion """


    with open('./tmp/transformation-cfvars/'+entorno+'/cloudDownloader-vars.json') as fichevars:
        variables = json.load(fichevars)
    variables["Parameters"]["DQ2Account"] = DATALAKEID
    with open('./tmp/transformation-cfvars/'+entorno+'/cloudDownloader-vars.json', 'w') as fichevars:
        json.dump(variables, fichevars, indent=1)


    with open('./tmp/transformation-cfvars/'+entorno+'/cloudUploader-vars.json') as fichevars:
        variables = json.load(fichevars)
    variables["Parameters"]["sqsOperatorEMail"] = credentials.OperatorEmailSNSServiceAlert
    variables["Parameters"]["anonymizationAwsAccountID"] = ANONYMIZATIONAWSACCOUNTID
    variables["Parameters"]["anonymizationAwsCanonicalId"] = ANONCANONICAL
    variables["Parameters"]["datalakeAwsCanonicalId"] = DATALAKECANONICAL
    variables["Parameters"]["transformationAwsCanonicalId"] = TRANSCANONICAL
    variables["Parameters"]["datalabAwsCanonicalId"] = DATALABAWSCANONICALACCOUNTID

    with open('./tmp/transformation-cfvars/'+entorno+'/cloudUploader-vars.json', 'w') as fichevars:
        json.dump(variables, fichevars, indent=1)



    with open('./tmp/transformation-cfvars/'+entorno+'/monitoring-vars.json') as fichevars:
        variables = json.load(fichevars)
    variables["Parameters"]["OperatorEmailSNSBillingMapfreInfraCloud"] = credentials.OperatorEmailSNSBillingMapfreInfraCloud
    variables["Parameters"]["OperatorEmailSNSBillingParadigma"] = credentials.OperatorEmailSNSBillingParadigma
    variables["Parameters"]["OperatorEmailAlert"] = credentials.OperatorEmailSNSServiceAlert
    variables["Parameters"]["AccountAWSAuditSec"] = ACCOUNTAWSAUDITSEC
    with open('./tmp/transformation-cfvars/'+entorno+'/monitoring-vars.json', 'w') as fichevars:
        json.dump(variables, fichevars, indent=1)


    with open('./tmp/transformation-cfvars/'+entorno+'/s3-vars.json') as fichevars:
        variables = json.load(fichevars)
    variables["Parameters"]["anonymizationAwsAccountID"] = ANONYMIZATIONAWSACCOUNTID
    variables["Parameters"]["transformationAwsAccountID"] = TRANSFORMATIONAWSACCOUNTID
    variables["Parameters"]["PROTransformationAwsAccountID"] = PROTRANSFORMATIONAWSACCOUNTID
    variables["Parameters"]["PROSecCicdAwsAccountID"] = PROSECCICDAWSACCOUNTID
    with open('./tmp/transformation-cfvars/'+entorno+'/s3-vars.json', 'w') as fichevars:
        json.dump(variables, fichevars, indent=1)

    if entorno == "pre":
        with open('./tmp/transformation-cfvars/'+entorno+'/network-vars.json') as fichevars:
            variables = json.load(fichevars)
        variables["Parameters"]["CidrVPC"] = network.transformationprevpc
        variables["Parameters"]["CidrSubnetPrivateAws1"] = network.transformationpreaz0
    if entorno == "dev":
        with open('./tmp/transformation-cfvars/'+entorno+'/network-vars.json') as fichevars:
            variables = json.load(fichevars)
        variables["Parameters"]["CidrVPC"] = network.transformationdevvpc
        variables["Parameters"]["CidrSubnetPrivateAws1"] = network.transformationdevaz0
        variables["Parameters"]["CidrSubnetPrivateAws2"] = network.transformationdevaz1
    if entorno == "pro":
        with open('./tmp/transformation-cfvars/'+entorno+'/network-vars.json') as fichevars:
            variables = json.load(fichevars)
        variables["Parameters"]["CidrVPC"] = network.transformationprovpc
        variables["Parameters"]["CidrSubnetPrivateAws1"] = network.transformationproaz0
        variables["Parameters"]["CidrSubnetPrivateAws2"] = network.transformationproaz1
        variables["Parameters"]["CidrSubnetPrivateAws3"] = network.transformationproaz2

    with open('./tmp/transformation-cfvars/'+entorno+'/network-vars.json', 'w') as fichevars:
        json.dump(variables, fichevars, indent=1)


def audit(entorno):
    """Funcion para auditoria """

    with open('./tmp/audit-cfvars/'+entorno+'/cloudtrail-vars.json') as fichevars:
        variables = json.load(fichevars)

    variables["Parameters"]["DatalakeAccount"] = DATALAKEID
    variables["Parameters"]["TransformationAccount"] = TRANSFORMATIONAWSACCOUNTID
    variables["Parameters"]["AnonymizationAccount"] = ANONYMIZATIONAWSACCOUNTID
    variables["Parameters"]["ProductivizationAccount"] = PRODUCAWSACCOUNTID
    variables["Parameters"]["Cdu1Account"] = CDU01AWSACCOUNTID
    #OJO HAY QUE CAMBIAR EL DE ABAJO, HABALR CON ALBERTO
    variables["Parameters"]["CICDACAccount"] = CDU01AWSACCOUNTID
    variables["Parameters"]["CommunicationAccount"] = COMMUNICATIONACCOUNTID
    with open('./tmp/audit-cfvars/'+entorno+'/cloudtrail-vars.json', 'w') as fichevars:
        json.dump(variables, fichevars, indent=1)



    with open('./tmp/audit-cfvars/'+entorno+'/monitoring-vars.json') as fichevars:
        variables = json.load(fichevars)
    variables["Parameters"]["OperatorEmailSNSBillingMapfreInfraCloud"] = credentials.OperatorEmailSNSBillingMapfreInfraCloud
    variables["Parameters"]["OperatorEmailSNSBillingParadigma"] = credentials.OperatorEmailSNSBillingParadigma
    variables["Parameters"]["OperatorEmailSNSSecurityAlert"] = credentials.OperatorEmailSNSSecurityAlert
    variables["Parameters"]["OperatorEmailSNSServiceAlert"] = credentials.OperatorEmailSNSServiceAlert
    variables["Parameters"]["OrganizationId"] = credentials.OrganizationId
    variables["Parameters"]["DatalakeAccount"] = DATALAKEID
    variables["Parameters"]["TransformationAccount"] = TRANSFORMATIONAWSACCOUNTID
    variables["Parameters"]["AnonymizationAccount"] = ANONYMIZATIONAWSACCOUNTID
    variables["Parameters"]["ProductivizationAccount"] = PRODUCAWSACCOUNTID
    variables["Parameters"]["Cdu1Account"] = CDU01AWSACCOUNTID
    variables["Parameters"]["CICDACAccount"] = CDU01AWSACCOUNTID
    variables["Parameters"]["DatalabAccount"] = DATALABAWSACCOUNTID


    with open('./tmp/audit-cfvars/'+entorno+'/monitoring-vars.json', 'w') as fichevars:
        json.dump(variables, fichevars, indent=1)
    if "pro" in entorno:
        with open('./tmp/audit-cfvars/'+entorno+'/network-vars.json') as fichevars:
            variables = json.load(fichevars)
        variables["Parameters"]["CidrVPC"] = network.auditvpc
        variables["Parameters"]["CidrSubnetPrivateAws1"] = network.auditaz0
        variables["Parameters"]["CidrSubnetPrivateAws2"] = network.auditaz1

        variables["Parameters"]["BucketTemplatesName"] = cf_template
        with open('./tmp/audit-cfvars/'+entorno+'/network-vars.json', 'w') as fichevars:
            json.dump(variables, fichevars, indent=1)



def cicd(entorno):
    """Funcion para cicd """
    with open('./tmp/ci-cd-cfvars/'+entorno+'/monitoring-vars.json') as fichevars:
        variables = json.load(fichevars)
    variables["Parameters"]["AccountAWSAuditSec"] = ACCOUNTAWSAUDITSEC
    variables["Parameters"]["OperatorEmailSNSBillingMapfreInfraCloud"] = credentials.OperatorEmailSNSBillingMapfreInfraCloud
    variables["Parameters"]["OperatorEmailSNSBillingParadigma"] = credentials.OperatorEmailSNSBillingParadigma
    variables["Parameters"]["OperatorEmailSNSServiceAlert"] = credentials.OperatorEmailSNSServiceAlert
    variables["Parameters"]["BucketTemplatesName"] = cf_template
    with open('./tmp/ci-cd-cfvars/'+entorno+'/monitoring-vars.json', 'w') as fichevars:
        json.dump(variables, fichevars, indent=1)
def casouso(entorno):
    """Funcion para caso de uso """
    with open('./tmp/usecase-cfvars/'+entorno+'/monitoring-vars.json') as fichevars:
        variables = json.load(fichevars)
    variables["Parameters"]["AccountAWSAuditSec"] = ACCOUNTAWSAUDITSEC
    variables["Parameters"]["OperatorEmailSNSBillingMapfreInfraCloud"] = credentials.OperatorEmailSNSBillingMapfreInfraCloud
    variables["Parameters"]["OperatorEmailSNSBillingParadigma"] = credentials.OperatorEmailSNSBillingParadigma
    variables["Parameters"]["OperatorEmailSNSServiceAlert"] = credentials.OperatorEmailSNSServiceAlert
    variables["Parameters"]["BucketTemplatesName"] = cf_template
    with open('./tmp/usecase-cfvars/'+entorno+'/monitoring-vars.json', 'w') as fichevars:
        json.dump(variables, fichevars, indent=1)



    if entorno == "pre":
        with open('./tmp/usecase-cfvars/'+entorno+'/network-vars.json') as fichevars:
            variables = json.load(fichevars)
        variables["Parameters"]["CidrVPC"] = network.casousoprevpc
        variables["Parameters"]["CidrSubnetPrivateAws1"] = network.casousopreaz0

    if entorno == "dev":
        with open('./tmp/usecase-cfvars/'+entorno+'/network-vars.json') as fichevars:
            variables = json.load(fichevars)
        variables["Parameters"]["CidrVPC"] = network.casousodevvpc
        variables["Parameters"]["CidrSubnetPrivateAws1"] = network.casousodevaz0
        variables["Parameters"]["CidrSubnetPrivateAws2"] = network.casousodevaz1
    if entorno == "pro":
        with open('./tmp/usecase-cfvars/'+entorno+'/network-vars.json') as fichevars:
            variables = json.load(fichevars)
        variables["Parameters"]["CidrVPC"] = network.casousoprovpc
        variables["Parameters"]["CidrSubnetPrivateAws1"] = network.casousoproaz0
        variables["Parameters"]["CidrSubnetPrivateAws2"] = network.casousoproaz1
        variables["Parameters"]["CidrSubnetPrivateAws3"] = network.casousoproaz2





    with open('./tmp/usecase-cfvars/'+entorno+'/network-vars.json', 'w') as fichevars:
        json.dump(variables, fichevars, indent=1)

def communication(entorno):
    """Funcion para comunicacion """
    with open('./tmp/communication-cfvars/'+entorno+'/monitoring-vars.json') as fichevars:
        variables = json.load(fichevars)
    variables["Parameters"]["AccountAWSAuditSec"] = ACCOUNTAWSAUDITSEC
    variables["Parameters"]["OperatorEmailSNSBillingMapfreInfraCloud"] = credentials.OperatorEmailSNSBillingMapfreInfraCloud
    variables["Parameters"]["OperatorEmailSNSBillingParadigma"] = credentials.OperatorEmailSNSBillingParadigma
    variables["Parameters"]["OperatorEmailSNSServiceAlert"] = credentials.OperatorEmailSNSServiceAlert
    with open('./tmp/communication-cfvars/'+entorno+'/monitoring-vars.json', 'w') as fichevars:
        json.dump(variables, fichevars, indent=1)


    with open('./tmp/communication-cfvars/'+entorno+'/network-vars.json') as fichevars:
        variables = json.load(fichevars)
    variables["Parameters"]["CidrVPC"] = network.communicationvpc
    variables["Parameters"]["CidrSubnetPublicAws1"] = network.communicationaz0
    variables["Parameters"]["CidrSubnetPublicAws2"] = network.communicationaz1
    variables["Parameters"]["CidrSubnetPublicAws3"] = network.communicationaz2
    variables["Parameters"]["CommunicationAccountId"] = COMMUNICATIONACCOUNTID
    variables["Parameters"]["TransformationAccountId"] = PROTRANSFORMATIONAWSACCOUNTID
    variables["Parameters"]["DatalabAccountId"] = DATALABAWSACCOUNTID
    variables["Parameters"]["cdudevAwsAccountID"] = CDUDEVTAWSACCOUNTID
    variables["Parameters"]["cdupreAwsAccountID"] = CDUPRETAWSACCOUNTID
    variables["Parameters"]["cduproAwsAccountID"] = CDUPROTAWSACCOUNTID
    




    with open('./tmp/communication-cfvars/'+entorno+'/network-vars.json', 'w') as fichevars:
        json.dump(variables, fichevars, indent=1)

def datalab(entorno):
    """Funcion para datalab """
    with open('./tmp/datalabs-cfvars/pro/emr-vars.json') as fichevars:
        variables = json.load(fichevars)
    variables["Parameters"]["iddatalakepro"] = DATALAKEID
    with open('./tmp/datalabs-cfvars/pro/emr-vars.json', 'w') as fichevars:
        json.dump(variables, fichevars, indent=1)


    with open('./tmp/datalabs-cfvars/pro/s3-vars.json') as fichevars:
        variables = json.load(fichevars)
    variables["Parameters"]["datalakeAwsAccountID"] = DATALAKEID
    with open('./tmp/datalabs-cfvars/pro/s3-vars.json', 'w') as fichevars:
        json.dump(variables, fichevars, indent=1)


    with open('./tmp/datalabs-cfvars/pro/monitoring-vars.json') as fichevars:
        variables = json.load(fichevars)


    variables["Parameters"]["AccountAWSAuditSec"] = ACCOUNTAWSAUDITSEC
    variables["Parameters"]["OperatorEmailSNSBillingMapfreInfraCloud"] = credentials.OperatorEmailSNSBillingMapfreInfraCloud
    variables["Parameters"]["OperatorEmailSNSBillingParadigma"] = credentials.OperatorEmailSNSBillingParadigma
    variables["Parameters"]["OperatorEmailSNSAlert"] = credentials.OperatorEmailSNSServiceAlert
    variables["Parameters"]["BucketTemplatesName"] = cf_template
    with open('./tmp/datalabs-cfvars/pro/monitoring-vars.json', 'w') as fichevars:
        json.dump(variables, fichevars, indent=1)


    if entorno == "pro":
        with open('./tmp/datalabs-cfvars/'+entorno+'/network-vars.json') as fichevars:
            variables = json.load(fichevars)
        variables["Parameters"]["CidrVPC"] = network.datalabvpc
        variables["Parameters"]["CidrSubnetPrivateAws1"] = network.datalabaz0
        variables["Parameters"]["CidrSubnetPrivateAws2"] = network.datalabaz1

        with open('./tmp/datalabs-cfvars/'+entorno+'/network-vars.json', 'w') as fichevars:
            json.dump(variables, fichevars, indent=1)

def clonerepo(source_url, local_repo_path):
    """Funcion para el clonado del repositorio """
    os.system("git clone "+source_url+" "+local_repo_path+";cd "+local_repo_path+";git checkout country_develop")

def workgroup (cred,nomeentorno,nomecuenta):
    session = boto3.Session(profile_name=cred)
    client = session.client("athena")
    if "pro" in nomeentorno:
        client.update_work_group(WorkGroup="primary",ConfigurationUpdates={'ResultConfigurationUpdates':{'OutputLocation':'s3://'+credentials.env+'-ac-bdaa-tmp-athena-results'},'EnforceWorkGroupConfiguration':True})
        try:            
            client.create_work_group(Name="scripts",Configuration={'ResultConfiguration':{'OutputLocation':'s3://'+credentials.env+'-ac-bdaa-tmp-athena-results'},'EnforceWorkGroupConfiguration':False})
        except Exception as e_error:
            lib_logs.redlog(str(e_error))

    else:
        try:
            client.create_work_group(Name="scripts",Configuration={'ResultConfiguration':{'OutputLocation':'s3://'+credentials.env+'-ac-bdaa-'+nomeentorno+'-tmp-athena-results'},'EnforceWorkGroupConfiguration':False})
        except Exception as e_error:
            lib_logs.redlog(str(e_error))
        client.update_work_group(WorkGroup="primary",ConfigurationUpdates={'ResultConfigurationUpdates':{'OutputLocation':'s3://'+credentials.env+'-ac-bdaa-'+nomeentorno+'-tmp-athena-results'},'EnforceWorkGroupConfiguration':True})

        

def getcanonical(cred):
    """Funcion para coger los id de las cuentas """
    try:
        nomeaccount = cred.split("_")[1]
        nomeenv = cred.split("_")[2]
        # cred=cred.split(" ")[1]
        session = boto3.Session(profile_name=cred)
        client = session.client("sts")
        account_id = client.get_caller_identity()["Account"]
        # lib_logs.greenlog(cred + " " + account_id)
        s3_list = session.client('s3')
        # lib_logs.greenlog(s3.list_buckets()['Owner']['ID'])
        canon = (s3_list.list_buckets()['Owner']['ID'])
        # diccanonicals={nomeaccount,nomeenv,account_id,canon}
        cf_region = (client.meta.region_name)
        listados3 = (s3_list.list_buckets())
        listalistado = (listados3['Buckets'])
        for i in listalistado:
            if 'cf-templates' in i["Name"]:
                cf_template = (i["Name"])
        diccanonicals = {"account":nomeaccount, "env":nomeenv, "id":account_id, "canonical":canon, "cf_template":cf_template, "cf_region":cf_region}
        LISTADIC.append(diccanonicals)
        print(nomeaccount, nomeenv, account_id, canon, cf_template, cf_region)
    except Exception as e_error:
        lib_logs.redlog(str(e_error))
def subidaentorno(local_repo_path):
    """Funcion para subida de los repos """
    pre = 'curl --user '+credentials.usuariobitbucket+':'+PASSBITBUCKET+' https://api.bitbucket.org/2.0/repositories/'+credentials.cadenapais+"/"+local_repo_path.split("/")[2]+'/pullrequests > temporal.json'

    os.system(pre)
    with open('./temporal.json') as fichevars:
        variables = json.load(fichevars)
    aprobado = (variables["values"][0]["links"]["approve"]["href"])
    saprobado = 'curl -X POST -u '+credentials.usuariobitbucket+':'+PASSBITBUCKET+' '+aprobado+' '
    os.system(saprobado)
    mergeado = (variables["values"][0]["links"]["merge"]["href"])
    smergeado = 'curl -X POST -u '+credentials.usuariobitbucket+':'+PASSBITBUCKET+' '+mergeado+' '
    os.system(smergeado)
def guardarepo(source_url, local_repo_path):
    """Funcion para guardar los repos """
    local_epoch1 = calendar.timegm(time.gmtime())
    local_epoch = str(local_epoch1)
    final = 'cd '+local_repo_path+';sleep 1; git checkout country_develop; git pull; git checkout -b feature/firstcommit'+local_epoch+'; git add .;git commit -m "rename all vars"; git push --set-upstream origin feature/firstcommit'+local_epoch+'; curl -X POST -H "Content-Type: application/json" -u '+credentials.usuariobitbucket+':'+PASSBITBUCKET+' https://api.bitbucket.org/2.0/repositories/'+credentials.cadenapais+"/"+local_repo_path.split("/")[2]+'/pullrequests/ -d ' + "'" + '{ "title": "Merge some branches", "description": "stackoverflow example", "source": { "branch": { "name": "feature/firstcommit'+local_epoch+'" }, "repository": { "full_name": "'+credentials.cadenapais+"/"+local_repo_path.split("/")[2]+'" } }, "destination": { "branch": { "name": "country_develop" } }, "close_source_branch": true }' + "'"
    os.system(final)
    try:
        subidaentorno(local_repo_path)
        parapre = 'curl -X POST -H "Content-Type: application/json" -u '+credentials.usuariobitbucket+':'+PASSBITBUCKET+' https://api.bitbucket.org/2.0/repositories/'+credentials.cadenapais+"/"+local_repo_path.split("/")[2]+'/pullrequests/ -d ' + "'" + '{ "title": "Merge some branches", "description": "stackoverflow example", "source": { "branch": { "name": "country_develop" }, "repository": { "full_name": "'+credentials.cadenapais+"/"+local_repo_path.split("/")[2]+'" } }, "destination": { "branch": { "name": "country_preproduction" } }, "close_source_branch": false }' + "'"
        os.system(parapre)
        subidaentorno(local_repo_path)
        paraprod = 'curl -X POST -H "Content-Type: application/json" -u '+credentials.usuariobitbucket+':'+PASSBITBUCKET+' https://api.bitbucket.org/2.0/repositories/'+credentials.cadenapais+"/"+local_repo_path.split("/")[2]+'/pullrequests/ -d ' + "'" + '{ "title": "Merge some branches", "description": "stackoverflow example", "source": { "branch": { "name": "country_preproduction" }, "repository": { "full_name": "'+credentials.cadenapais+"/"+local_repo_path.split("/")[2]+'" } }, "destination": { "branch": { "name": "country_production" } }, "close_source_branch": false }' + "'"
        os.system(paraprod)
        subidaentorno(local_repo_path)
    except:
        lib_logs.bluelog("HA FALLADO ALGO " + local_repo_path)

#MAIN

FILEREPOS = open("repos.txt", "r+").read().split('\n')
for namerepo in FILEREPOS:
    bitbucketURL = 'git@bitbucket.org:'+credentials.cadenapais+'/'+namerepo+'.git'
    #comentado para no perder tiuempo en crear los repos cada prueba, descomentar cuando se ponga a prod
    clonerepo(bitbucketURL, "./tmp/"+namerepo)

FILECONFIGAWS = open(HOME+"/.aws/config", "r+").read().split('\n')

for nameconfig in FILECONFIGAWS:
    if ("[profile" in nameconfig) and ("default" or "Default" not in nameconfig):
        if credentials.env in nameconfig:
            nomeprofile = nameconfig.split(" ")[1].split("]")[0]
            nomecuenta = nomeprofile.split("_")[1]
            nomeentorno = nomeprofile.split("_")[2]          
            getcanonical(nomeprofile)
            if "datalake" in nomecuenta:
                workgroup(nomeprofile,nomeentorno,nomecuenta)

            
#datalake
PROSECCICDAWSACCOUNTID = listado("seccicd", "pro").get("id")
DATALABAWSACCOUNTID = listado("datalab", "pro").get("id")
PRODATALAKEAWSACCOUNTID = listado("datalake", "pro").get("id")
DATALABAWSCANONICALACCOUNTID = listado("datalab", "pro").get("canonical")

PROTRANSFORMATIONAWSACCOUNTID = listado("transformation", "pro").get("id")
CDUDEVTAWSACCOUNTID = listado("secaudit", "dev").get("id")
CDUPRETAWSACCOUNTID = listado("secaudit", "pre").get("id")
CDUPROTAWSACCOUNTID = listado("secaudit", "pro").get("id")



#datalab = listado("datalab", "pro").get("id")


for entornobucle in ["dev", "pre", "pro"]:
    ANONYMIZATIONAWSACCOUNTID = listado("anonymization", entornobucle).get("id")
    TRANSFORMATIONAWSACCOUNTID = listado("transformation", entornobucle).get("id")
    CDU01AWSACCOUNTID = listado("cdu", entornobucle).get("id")
    ACCOUNTAWSAUDITSEC = listado("secaudit", entornobucle).get("id")
    cf_template = listado('datalake', entornobucle).get('cf_template')
    regiondatalake = listado('datalake', entornobucle).get('cf_region')
    datalake(entornobucle)

#Anonmyzation



for entornobucle in ["dev", "pre", "pro"]:
    TRANSFORMATIONAWSACCOUNTID = listado("transformation", entornobucle).get("id")
    ANONCANONICAL = listado("anonymization", entornobucle).get("canonical")
    DATALAKECANONICAL = listado("datalake", entornobucle).get("canonical")
    ACCOUNTAWSAUDITSEC = listado("secaudit", entornobucle).get("id")
    cf_template = listado('anonymization', entornobucle).get('cf_template')


    anonymization(entornobucle)

#Transformation

for entornobucle in ["dev", "pre", "pro"]:

    ANONYMIZATIONAWSACCOUNTID = listado("anonymization", entornobucle).get("id")
    ANONCANONICAL = listado("anonymization", entornobucle).get("canonical")
    DATALAKECANONICAL = listado("datalake", entornobucle).get("canonical")
    DATALAKEID = listado("datalake", entornobucle).get("id")
    TRANSCANONICAL = listado("transformation", entornobucle).get("canonical")
    ACCOUNTAWSAUDITSEC = listado("secaudit", entornobucle).get("id")
    TRANSFORMATIONAWSACCOUNTID = listado("transformation", entornobucle).get("id")
    cf_template = listado('transformation', entornobucle).get('cf_template')
    transformacion(entornobucle)
#Audit

for entornobucle in ["dev", "pre", "pro"]:
    ANONYMIZATIONAWSACCOUNTID = listado("anonymization", entornobucle).get("id")
    DATALAKEID = listado("datalake", entornobucle).get("id")
    TRANSFORMATIONAWSACCOUNTID = listado("transformation", entornobucle).get("id")
    PRODUCAWSACCOUNTID = listado("productivization", entornobucle).get("id")
    CDU01AWSACCOUNTID = listado("cdu", entornobucle).get("id")
    COMMUNICATIONACCOUNTID = listado("communication", entornobucle).get("id")
    cf_template = listado('secaudit', entornobucle).get('cf_template')
    DATALABAWSACCOUNTID = listado("datalab", "pro").get("id")

    audit(entornobucle)

#CICD
ACCOUNTAWSAUDITSEC = listado("secaudit", "pro").get("id")
cf_template = listado('seccicd', "pro").get('cf_template')
cicd("pro")

#Comunication
ACCOUNTAWSAUDITSEC = listado("secaudit", "pro").get("id")
cf_template = listado('communication', "pro").get('cf_template')
COMMUNICATIONACCOUNTID = listado("communication", "pro").get("id")

communication("pro")

#Datalab
DATALAKEID = listado("datalake", "pro").get("id")
ACCOUNTAWSAUDITSEC = listado("secaudit", "pro").get("id")
#cf_template = listado('datalab', entorno).get('cf_template')
datalab("pro")

#Caso de uso
for entornobucle in ["dev", "pre", "pro"]:
    ACCOUNTAWSAUDITSEC = listado("secaudit", entornobucle).get("id")
    casouso(entornobucle)


#guardar repos
for namerepo in FILEREPOS:
    bitbucketURL = 'git@bitbucket.org:'+credentials.cadenapais+'/'+namerepo+'.git'
    guardarepo(bitbucketURL, "./tmp/"+namerepo)

# #BORRADO
os.system("rm -rf ./tmp/*")
os.system("rm temporal.json")
