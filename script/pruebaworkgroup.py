import os
import json
import calendar
import time
from pathlib import Path
import boto3
import libs.lib_logs as lib_logs
import credentials
import network


def workgroup (entornobucle,regiondatalake):
    session = boto3.Session(profile_name="br_datalake_pre")

    client = session.client("sts")
    cf_region = (client.meta.region_name)
    print (cf_region)

    client = session.client("athena")

    client.update_work_group(WorkGroup="primary",ConfigurationUpdates={'ResultConfigurationUpdates':{'OutputLocation':'s3://br-ac-bdaa-pre-tmp-athena-results'},'EnforceWorkGroupConfiguration':True})
    # account_id = client.get_caller_identity()["Account"]

workgroup("dev","eu-west-1")
