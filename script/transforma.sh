#!/usr/bin/python3

import sys
import json

try:
    file_input = sys.argv[1]
except:
    print("Please specify an input file")
    exit(1)
print ("uno")
with open(file_input) as f:
    print (f)
    parameters = json.load(f)

output = []
for param in parameters["Parameters"]:
    output.append({
        "ParameterKey": param,
        "ParameterValue": parameters["Parameters"][param]
    })

f = open("output.json", "w")
f.write(str(output).replace("'", '"'))
f.close()