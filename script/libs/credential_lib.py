import libs.lib_logs as lib_logs
import os
import sys
import getopt
import boto3
import re

def help():
    print(''' Main lib''')
        
def get_modules(profiles_list):
    new_list = []
    try:
        for i in profiles_list:
            module = i.split("_")[1]
            if module not in new_list:
                new_list.append(module)
    except:
        for i in profiles_list:
            module = i.split(".")[ len(i.split("."))-2 ]
            if module not in new_list:
                new_list.append(module)
    new_list.sort()
    new_list.append("all")
    lib_logs.bluelog("Select module " + str(new_list) + ": ")
    selected_module = input()
    
    if selected_module == "all":
        return(profiles_list)
    else:
        selected_list = []
        for profile in profiles_list:
            if str(selected_module) in profile:
                selected_list.append(profile)
        return(selected_list)
    
def get_profiles(home_dir):
    profiles_list = []
    aws_cred_path = ".aws/config"
    aws_cred_file = os.path.join(home_dir,aws_cred_path)
    if os.path.isfile(aws_cred_file):
        f = open( aws_cred_file , "r" )
        profiles = f.readlines()
        f.close()
        for line in profiles:
            fixed_line = line.strip()
            m = re.match("\[(\w+)_(\w+)_(\w+)\]",fixed_line)
            if m:
                fixed_line = fixed_line.replace('[', '')
                fixed_line = fixed_line.replace(']', '')
                profiles_list.append(fixed_line)
            n = re.match("\[(\w+).(\w+).(\w+).(\w+)\]",fixed_line)
            if n:
                fixed_line = fixed_line.replace('[', '')
                fixed_line = fixed_line.replace(']', '')
                profiles_list.append(fixed_line)
            p = re.match("\[(\w+).(\w+).(\w+).(\w+).(\w+)\]",fixed_line)
            if p:
                fixed_line = fixed_line.replace('[', '')
                fixed_line = fixed_line.replace(']', '')
                profiles_list.append(fixed_line)
    else:
        lib_logs.redlog("AWS configuration not found.")
        exit(1)
    return(profiles_list)
    
def get_country():
    home_dir = os.environ['HOME']
    profiles_list = get_profiles(home_dir)
    countries = []
    for profile in profiles_list:
        if profile.startswith("aws."):
            country = profile.split(".")[ len(profile.split("."))-1 ]
        else:
            country = profile.split("_")[0]
        if country not in countries:
            countries.append(country)
    return(countries)
    
def get_account_id(cred):
    session = boto3.Session(profile_name=cred)
    client = session.client("sts")
    account_id = client.get_caller_identity()["Account"]
    return(account_id)
    
def selection_profiles(country):
    home_dir = os.environ['HOME']
    profiles_list = get_profiles(home_dir)
    profiles_selection = []
    for profile in profiles_list:
        if str(country) in str(profile):
            profiles_selection.append(profile)
    return(profiles_selection)
    
def get_credentials():
    lib_logs.bluelog("Select country " + str(get_country()) +": ")
    selection_country = input()
    selected_profiles = selection_profiles(selection_country)
    selected_module = get_modules(selected_profiles)
    return(selected_module, selection_country)
    
def getModuleAndEnviron(profile):
    if profile.startswith("aws."):
        country = profile.split(".")[ len(profile.split("."))-1 ]
        module = profile.split(".")[ len(profile.split("."))-2 ]
        environ = profile.split(".")[1]
    else:
        country = profile.split("_")[0]
        module = profile.split("_")[1]
        environ = profile.split("_")[2]
    return country, module, environ

