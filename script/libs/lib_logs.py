import os
import sys
import re


colors = {
"reset": "\033[0m",
"red": "\033[1;31m",
"yellow": "\033[0;93m",
"green": "\033[0;92m",
"blue": "\033[0;94m"
}

def help():
    print(''' Main lib''')
        
        
def redlog(text):
    print(colors['red'] + str(text) + colors['reset'])

def bluelog(text):
    print(colors['blue'] + str(text) + colors['reset'])
    
def yellowlog(text):
    print(colors['yellow'] + str(text) + colors['reset'])
    
def greenlog(text):
    print(colors['green'] + str(text) + colors['reset'])
    
