import libs.lib_logs as lib_logs
import os
import sys
import getopt
import boto3
import re

def help():
    print(''' Main lib''')

def read_config_file(country, module, environ, root_path):
    profile = country + "_" + module + "_" + environ
    lib_logs.greenlog ("Reading config file..." + profile)
    config_file = root_path + "/cloudformation/variables/" + country + "/" + environ + "/" + profile + ".config"
    f = open( config_file , "r" )
    stack_parameters = f.readlines()
    f.close()
    params_list = []
    for line in stack_parameters:
        try:
            fixed_line = line.strip()
            fixed_line = fixed_line.replace('{', '')
            fixed_line = fixed_line.replace('}', '')
            ParameterKey = fixed_line.split(', ')[0].split("'")[3]
            ParameterValue = fixed_line.split(', ')[1].split("'")[3]
        except Exception as e:
            lib_logs.redlog(str(e))
        params_list.append( { 'ParameterKey': ParameterKey, 'ParameterValue': ParameterValue} )
    return(params_list)
    
def read_file(root_path):
    lib_logs.greenlog("Reading file..." + root_path)
    f = open( root_path , "r" )
    readed_file = f.readlines()
    f.close()
    file_string = ""
    for line in readed_file:
        file_string += str(line.strip())
    return(file_string)
        
def clean_readed_file(readed_file_str):
    file_string = ""
    for line in readed_file_str:
        file_string += str(line.strip())
    return(file_string)
