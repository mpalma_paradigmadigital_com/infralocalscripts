import sys
import os
import getopt
import json
import pycurl

try:
    from StringIO import StringIO
except ImportError:
    from io import StringIO
try:
    from BytesIO import BytesIO
except ImportError:
    from io import BytesIO

def help():
    print(""" Library for bitbucket """)


def readurl(url, user, passw):
    try:
        try:
            response = StringIO()
            c = pycurl.Curl()
            c.setopt(pycurl.USERPWD, user + ":" + passw)
            c.setopt(c.URL, url)
            c.setopt(c.WRITEFUNCTION, response.write)
            c.perform()
            c.close()
            message = response.getvalue()
            response.close()
        except:
            response = BytesIO()
            c = pycurl.Curl()
            c.setopt(pycurl.USERPWD, user + ":" + passw)
            c.setopt(c.URL, url)
            c.setopt(c.WRITEFUNCTION, response.write)
            c.perform()
            c.close()
            message = response.getvalue()
            response.close()
    except Exception as e:
        print("Unexpected error: %s" % e)
        raise e
        
    return (message)

def get_repositories(user, passw):
    
    bitbucket_team = input("Write the bitbucket team: ")
        
    url = 'https://api.bitbucket.org/2.0/repositories/' + bitbucket_team + "?pagelen=100"
    
    message = readurl(url, user, passw)
        
    readedjson = json.loads(message)
    size = int(readedjson['size'])
    pagelen = int(readedjson['pagelen'])
    pages = int( (size/pagelen)+1 )
    
    newmessage = []
    for i in range (1, pages+1):
        url = "https://api.bitbucket.org/2.0/repositories/"+bitbucket_team+"?pagelen=100&page=" + str(i)
        newmessage.append(readurl(url, user, passw))
    
    repositories_list = []
    for j in newmessage:
        readedjson = json.loads(j)
        for i in readedjson['values']:
            repositories_list.append(i['name'])
            
    return (bitbucket_team, repositories_list)
    
def get_team_repositories(user, passw, bitbucket_team):
        
    url = 'https://api.bitbucket.org/2.0/repositories/' + bitbucket_team + "?pagelen=100"
    
    message = readurl(url, user, passw)
        
    readedjson = json.loads(message)
    size = int(readedjson['size'])
    pagelen = int(readedjson['pagelen'])
    pages = int( (size/pagelen)+1 )
    
    newmessage = []
    for i in range (1, pages+1):
        url = "https://api.bitbucket.org/2.0/repositories/"+bitbucket_team+"?pagelen=100&page=" + str(i)
        newmessage.append(readurl(url, user, passw))
    
    repositories_list = []
    for j in newmessage:
        readedjson = json.loads(j)
        for i in readedjson['values']:
            repositories_list.append(i['name'])
            
    return (bitbucket_team, repositories_list)
    
def get_branch_restrictions(bitbucket_team, reponame, user, passw):
    
    url = 'https://api.bitbucket.org/2.0/repositories/'+bitbucket_team+'/'+reponame+'/branch-restrictions?pagelen=100'
    message = readurl(url, user, passw)
    readedjson = json.loads(message)
    size = int(readedjson['size'])
    pagelen = int(readedjson['pagelen'])
    pages = int( (size/pagelen)+1 )
    newmessage = []
    for num in range (1, pages+1):
        url = "https://api.bitbucket.org/2.0/repositories/"+bitbucket_team+"/"+reponame+"/branch-restrictions?pagelen=100&page=" + str(num)
        newmessage.append(readurl(url, user, passw))
        
    restriction_ids = []
    for message in newmessage:
        readedjson = json.loads(message)
        for value in readedjson['values']:
            restriction_ids.append(value)
            
    return (restriction_ids)
    
def read_message(message):
    readedjson = json.loads(message)
    return(readedjson)
        
def curl_post(url, user, passw, postfields):
    c = pycurl.Curl()
    c.setopt(pycurl.USERPWD, user + ":" + passw)
    c.setopt(c.URL, url)
    c.setopt(c.HTTPHEADER, ['content-type: application/json'])
    #fake function to avoid console output
    c.setopt(c.WRITEFUNCTION, lambda x: None)
    c.setopt(c.POSTFIELDS, postfields)
    c.perform()
    c.close()
    
def curl_post_file(url, user, passw, postfields):
    c = pycurl.Curl()
    c.setopt(pycurl.USERPWD, user + ":" + passw)
    c.setopt(c.URL, url)
    c.setopt(c.HTTPHEADER, ['content-type: application/x-www-form-urlencoded'])
    #fake function to avoid console output
    c.setopt(c.WRITEFUNCTION, lambda x: None)
    c.setopt(c.POSTFIELDS, postfields)
    c.perform()
    c.close()
    print("OK")
    
def curl_delete(url, user, passw):
    c = pycurl.Curl()
    c.setopt(pycurl.USERPWD, user + ":" + passw)
    c.setopt(c.URL, url)
    c.setopt(c.CUSTOMREQUEST, "DELETE")
    c.perform()
    c.close()

