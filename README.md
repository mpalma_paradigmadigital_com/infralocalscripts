Scripts locales para crear las redes a partir de un bloque CIDR y las variables de todas las cuentas para cada pais.

El enlace con la documentacion se encuentra en el siguiente enlace:

EJ: python3.6 clonerepos.py. 
EJ: python3.6 createnetwork.py.

[Manual de scripts locales](https://mapfrealm.atlassian.net/wiki/spaces/ATENEA/pages/876445864/Scripts+locales+de+Infra)
